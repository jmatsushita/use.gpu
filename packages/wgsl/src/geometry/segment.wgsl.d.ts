declare module "@use-gpu/wgsl/geometry/segment.wgsl" {
  type ParsedBundle = import('@use-gpu/shader').ParsedBundle;
  const __module: ParsedBundle;
  export const getLineSegment: ParsedBundle;
  export default __module;
}
