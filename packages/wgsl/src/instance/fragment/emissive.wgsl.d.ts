declare module "@use-gpu/wgsl/instance/fragment/emissive.wgsl" {
  type ParsedBundle = import('@use-gpu/shader').ParsedBundle;
  const __module: ParsedBundle;
  export const getEmissiveFragment: ParsedBundle;
  export default __module;
}
