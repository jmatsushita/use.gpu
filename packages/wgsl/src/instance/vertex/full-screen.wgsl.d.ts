declare module "@use-gpu/wgsl/instance/vertex/full-screen.wgsl" {
  type ParsedBundle = import('@use-gpu/shader').ParsedBundle;
  const __module: ParsedBundle;
  export const getFullScreenVertex: ParsedBundle;
  export default __module;
}
