declare module "@use-gpu/wgsl/pmrem/pmrem-blur.wgsl" {
  type ParsedBundle = import('@use-gpu/shader').ParsedBundle;
  const __module: ParsedBundle;
  export const pmremBlur: ParsedBundle;
  export default __module;
}
