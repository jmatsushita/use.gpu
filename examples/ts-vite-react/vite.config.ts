import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'
import wasm from "vite-plugin-wasm";
import topLevelAwait from "vite-plugin-top-level-await";
import wsgl from "@use-gpu/wgsl-loader/rollup";

// https://vitejs.dev/config/
export default defineConfig({
  optimizeDeps: {
    exclude: ["@use-gpu/workbench"],
    esbuildOptions: {
      target: "esnext",
    },
  },
  plugins: 
    [ react(),
      wasm(),
      topLevelAwait(),
      wsgl()
    // , {
    //     ...wsgl(),
    //     enforce: 'pre',
    //     // apply: 'serve',
    //   }
    ],
})
