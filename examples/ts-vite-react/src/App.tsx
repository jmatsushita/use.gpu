import { useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import './App.css'

import { LiveCanvas } from '@use-gpu/react';

import type { LC, PropsWithChildren } from '@use-gpu/live';
// import { Component } from './Component';

import { WebGPU, AutoCanvas } from '@use-gpu/webgpu';
import { OrbitControls, OrbitCamera, Pass} from '@use-gpu/workbench';
import { UI, Layout, Flex, Block, Inline, Text } from '@use-gpu/layout';

type ComponentProps = {
  canvas: HTMLCanvasElement,
};

const WHITE = [1, 1, 1, 1];

// This is a Live component
export const Component: LC<ComponentProps> = (props: PropsWithChildren<ComponentProps>) => {
  const {canvas} = props;
  return (
    <WebGPU 
    // fallback={(error: Error) => <div>Error: {JSON.stringify(error)}</div>}
    >
      <AutoCanvas canvas={canvas}>
        <Camera>
          <Pass lights>
            <UI>
              <Layout placement="center">
                <Flex 
                  direction="y" 
                  anchor={"center"} 
                  align={"center"} 
                  height={'100%'}
                  >
                  <Block width={1400}>
                    <Block margin={20}>
                      <Inline align={"center"}>
                        <Text
                          size={32}
                          detail={64}
                          snap={false}
                          text={"Hello World"}
                          color={WHITE}
                        />
                      </Inline>
                    </Block>
                  </Block>
                </Flex>
              </Layout>
            </UI>
          </Pass>
        </Camera>
      </AutoCanvas>
    </WebGPU>
  );
};

const Camera = ({children}: PropsWithChildren<object>) => (
  <OrbitControls
    radius={5}
    bearing={0.5}
    pitch={0.3}
    render={(radius: number, phi: number, theta: number, target: vec3) =>
      <OrbitCamera
        radius={radius}
        phi={phi}
        theta={theta}
        target={target}
      >
        {children}
      </OrbitCamera>
    }
  />
);
function App() {
  const [count, setCount] = useState(0)

  return (
    <>
      <div>
        <a href="https://vitejs.dev" target="_blank">
          <img src={viteLogo} className="logo" alt="Vite logo" />
        </a>
        <a href="https://react.dev" target="_blank">
          <img src={reactLogo} className="logo react" alt="React logo" />
        </a>
      </div>
      <h1>Vite + React</h1>
      <LiveCanvas>
        {(canvas) => <Component canvas={canvas} />}
      </LiveCanvas>
      <div className="card">
        <button onClick={() => setCount((count) => count + 1)}>
          count is {count}
        </button>
        <p>
          Edit <code>src/App.tsx</code> and save to test HMR
        </p>
      </div>
      <p className="read-the-docs">
        Click on the Vite and React logos to learn more
      </p>
    </>
  )
}

export default App
