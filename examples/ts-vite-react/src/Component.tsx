// import type { LC, PropsWithChildren } from '@use-gpu/live';
// import type { GPUGeometry, DataField } from '@use-gpu/core';

// import React, { use } from '@use-gpu/live';
// import { vec3 } from 'gl-matrix';

// import {
//   Pass,
//   Cursor,
//   GeometryData,
//   OrbitCamera, OrbitControls,
//   FaceLayer, RawQuads,
//   makeBoxGeometry,
//   Animate,
//   PointLight,
//   LinearRGB,
// } from '@use-gpu/workbench';

// import { Scene, Node, Mesh, Primitive } from '@use-gpu/scene';
// import { UI, Layout, Flex, Block, Inline, Text } from '@use-gpu/layout';
// import { Cartesian, Grid, Axis, Scale, Tick, Label, Sampled, Surface, Line, Transpose } from '@use-gpu/plot/src';

// // Prefab geometry
// const boxGeometry = makeBoxGeometry({ width: 50 });
// const WHITE = [1, 1, 1, 1];
// const BACKGROUND = [0, 0, 0, 1];

// // Positions of board's lines.
// const lines = [
//   ...[...Array(19).keys()].map((i) => ({  
//     start: [-9+i, -9, 0],
//     end: [-9+i, 9, 0]
//   })),
//   ...[...Array(19).keys()].map((i) => ({  
//     start: [-9, -9+i, 0],
//     end: [9, 9+i, 0]
//   })),
// ];

// const Camera = ({children}: PropsWithChildren<object>) => (
//   <OrbitControls
//     // radius={3}
//     // bearing={0.5}
//     // pitch={0.3}
//     radius={1000}
//     moveSpeed={1/1000}
//     // bearing={0.3}
//     // pitch={0.5}
//     render={(radius: number, phi: number, theta: number, target: vec3) =>
//       <OrbitCamera
//         radius={radius}
//         phi={phi}
//         theta={theta}
//         target={target}
//         near={0.1}
//         far={20000}
//       >
//         {children}
//       </OrbitCamera>
//     }
//   />
// );


// export const Component: LC = () => {

//   return (
//     <Camera>
//       <Cursor cursor='move' />
//       <LinearRGB backgroundColor={BACKGROUND}>
//         <Pass lights>
//           {/* <UI>
//             <Layout placement="center">
//               <Flex 
//                 direction="y" 
//                 anchor={"center"} 
//                 align={"center"} 
//                 height={'100%'}
//                 >
//                 <Block width={1400}>
//                   <Block margin={20}>
//                     <Inline align={"center"}>
//                       <Text
//                         size={32}
//                         detail={64}
//                         snap={false}
//                         text={"Some UI Text"}
//                         color={WHITE}
//                       />
//                     </Inline>
//                   </Block>
//                 </Block>
//               </Flex>
//             </Layout>
//           </UI> */}
//           <Animate
//             loop
//             delay={0}
//             keyframes={[
//               [0, [30, 20, 10]],
//               [4, [20, 10, 40]],
//               [8, [-5, 20, 20]],
//               [12, [30, 20, 10]],
//             ]}
//             prop='position'
//           >
//             <PointLight position={[10, 20, 30]} color={[0.5, 0.0, 0.25]} intensity={40*40} />
//           </Animate>
//           <Scene>
//             <Node position={[-50, 0, 0]}>
//               <GeometryData
//                 {...boxGeometry}
//                 render={(mesh: GPUGeometry) => 
//                   <Mesh shaded
//                     {...({
//                       ...mesh, 
//                       // mode: 'debug'
//                     } as any)}
//                   />
//                 }
//               />
//             </Node>
//             <Node position={[.5, 0, 0]}>
//               <Primitive>
//                 <RawQuads
//                   count={1}
//                   position={[0, 0, 0, 1]}
//                   rectangle={[-5000, -4000, 5000, 4000]}
//                   depth={1}
//                   mode="debug"
//                 />
//               </Primitive>
//             </Node>
//             <Node position={[60, 0, 0]}>
//               <Primitive>
//               {/* 2D Layout */}
//                 <UI>
//                   <Layout placement="center">
//                     <Flex 
//                       direction="y" 
//                       // anchor={"center"} 
//                       // align={"center"} 
//                       // height={'100%'}
//                     >
//                       <Block width={500}>
//                         <Block 
//                           margin={40}
//                           >
//                           <Inline align={"center"}>
//                             <Text
//                               size={32}
//                               detail={64}
//                               snap={false}
//                               text={"Go in some Depth"}
//                               color={WHITE}
//                             />
//                           </Inline>
//                         </Block>
//                         <Block margin={20}>
//                           <Inline align={"center"}>
//                             <Text
//                               // size={SIZE}
//                               // detail={DETAIL}
//                               // lineHeight={height}
//                               snap={false}
//                               text="I imagine a go board, that unfolds. so that the sides are an infinite line. From this perspective (and putting the centre and corners aside for now), the 10 lines from the border to the centre, are superimposed as an altitude, a level. Something really peculiar emerges when you play games after games... "
//                               color={WHITE}
//                             />
//                           </Inline>
//                         </Block>
//                         <Block width={500}>
//                           <Block margin={500}>
//                             <Inline align={"center"}>
//                               <Text
//                                 // size={SIZE}
//                                 // detail={DETAIL}
//                                 // lineHeight={height}
//                                 snap={false}
//                                 text={JSON.stringify(lines)}
//                                 color={WHITE}
//                               />
//                             </Inline>
//                           </Block>
//                         </Block>
//                       </Block>
//                     </Flex>
//                   </Layout>                
//                 </UI>
//               </Primitive>
//             </Node>

//             <Cartesian
//               scale={[190, 190, 19]}
//             >
//               <Grid
//                 axes='xy'
//                 width={2}
//                 first={{ detail: 1, divide: 10 }}
//                 second={{ detail: 1, divide: 10 }}
//                 depth={0.5}
//                 zBias={-1}
//                 // auto
//               />
//               {/* <Grid
//                 axes='xz'
//                 width={2}
//                 first={{ detail: 3, divide: 5 }}
//                 second={{ detail: 3, divide: 5 }}
//                 depth={0.5}
//                 zBias={-1}
//               />
//               <Grid
//                 axes='yz'
//                 width={2}
//                 first={{ detail: 3, divide: 5 }}
//                 second={{ detail: 3, divide: 5, end: true }}
//                 depth={0.5}
//                 zBias={-1}
//                 auto
//               /> */}

//               {/* <Axis
//                 axis='x'
//                 width={5}
//                 color={[0.75, 0.75, 0.75, 1]}
//                 depth={0.5}
//               />
//               <Scale
//                 divide={5}
//                 axis='x'
//               >
//                 <Tick
//                   size={20}
//                   width={5}
//                   offset={[0, 1, 0]}
//                   color={[0.75, 0.75, 0.75, 1]}
//                   depth={0.5}
//                 />
//                 <Label
//                   placement='bottom'
//                   color='#80808080'
//                   size={24}
//                   offset={16}
//                   expand={5}
//                   depth={0.5}
//                 />
//                 <Label
//                   placement='bottom'
//                   color='#ffffff'
//                   size={24}
//                   offset={16}
//                   expand={0}
//                   depth={0.5}
//                 />
//               </Scale> */}

//               <Axis
//                 axis='y'
//                 width={5}
//                 color={[0.75, 0.75, 0.75, 1]}
//                 detail={8}
//                 depth={0.5}
//               />
//               <Scale
//                 divide={19}
//                 axis='y'
//               >
//                 <Tick
//                   size={20}
//                   width={5}
//                   offset={[0, 1, 0]}
//                   color={[0.75, 0.75, 0.75, 1]}
//                   depth={0.5}
//                 />
//                 <Label
//                   placement='bottom'
//                   color='#80808080'
//                   size={24}
//                   offset={16}
//                   expand={5}
//                   depth={0.5}
//                 />
//                 <Label
//                   placement='bottom'
//                   color='#ffffff'
//                   size={24}
//                   offset={16}
//                   expand={0}
//                   depth={0.5}
//                 />
//               </Scale>

//               {/* <Axis
//                 axis='z'
//                 width={5}
//                 color={[0.75, 0.75, 0.75, 1]}
//                 detail={8}
//                 depth={0.5}
//               /> */}

//               <Sampled
//                 axes='zx'
//                 format='vec4<f32>'
//                 size={[10, 20]}
//                 expr={(emit, z, x) => {
//                   const v = Math.cos(x) * Math.cos(z);
//                   emit(x, v * .4 + .5, z, 1);
//                 }}
//               >
//                 <Surface
//                   color={[0.1, 0.3, 1, 1]}
//                 />
//                 <Line
//                   width={2}
//                   color={[0.5, 0.5, 1, 0.5]}
//                   depth={0.5}
//                   zBias={1}
//                 />
//                 <Transpose axes='yx'>
//                   <Line
//                     width={2}
//                     color={[0.5, 0.5, 1, 0.5]}
//                     depth={0.5}
//                     zBias={1}
//                   />
//                 </Transpose>
//               </Sampled>
//             </Cartesian>
//           </Scene>
//         </Pass>
//       </LinearRGB>
//     </Camera>
//   );
// };
